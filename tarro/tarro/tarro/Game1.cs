using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


//Some code used from xnafan.net as a tutorial, thanks man!


namespace Game1
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private Texture2D tileTexture;
        private Texture2D playerTexture;

        private Player jumper;
        private Board board;

        private Random random = new Random();

        private SpriteFont debugFont;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 1600;
            graphics.PreferredBackBufferHeight = 900;
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            tileTexture = Content.Load<Texture2D>("Graphics/tile");
            playerTexture = Content.Load<Texture2D>("Graphics/player");

            jumper = new Player(playerTexture, new Vector2(80, 80), spriteBatch);

            board = new Board(spriteBatch, tileTexture, 50, 28);

            debugFont = Content.Load<SpriteFont>("DebugFont");
        }

        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }

            base.Update(gameTime);
            jumper.Update(gameTime);
            CheckKeyboardAndReact();
        }

        private void CheckKeyboardAndReact()
        {
            KeyboardState state = Keyboard.GetState();

            if (state.IsKeyDown(Keys.F5))
            {
                RestartGame();
            }

            if (state.IsKeyDown(Keys.Escape)) 
            { 
                Exit(); 
            }
        }

        private void RestartGame()
        {
            Board.CurrentBoard.CreateNewBoard();
            PutJumperInTopLeftCorner();
        }

        private void PutJumperInTopLeftCorner()
        {
            jumper.Position = Vector2.One * 80;
            jumper.Movement = Vector2.Zero;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.WhiteSmoke);
            spriteBatch.Begin();
            base.Draw(gameTime);
            board.Draw();
            WriteDebugInformation();
            jumper.Draw();
            spriteBatch.End();
        }

        private void WriteDebugInformation()
        {
            string positionInText = string.Format("Position of Jumper: ({0:0.0}, {1:0.0})", jumper.Position.X, jumper.Position.Y);
            string movementInText = string.Format("Current movement: ({0:0.0}, {1:0.0})", jumper.Movement.X, jumper.Movement.Y);
            string isOnFirmGroundText = string.Format("On firm ground? : {0}", jumper.IsOnFirmGround());

            DrawWithShadow(positionInText, new Vector2(10, 0));
            DrawWithShadow(movementInText, new Vector2(10, 20));
            DrawWithShadow(isOnFirmGroundText, new Vector2(10, 40));
            DrawWithShadow("F5 for random board", new Vector2(70, 600));
        }

        private void DrawWithShadow(string text, Vector2 position)
        {
            spriteBatch.DrawString(debugFont, text, position + Vector2.One, Color.Black);
            spriteBatch.DrawString(debugFont, text, position, Color.LightYellow);
        }
    }
}